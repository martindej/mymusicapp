#include <iostream>
#include <stdio.h>
#include <fstream>
#include <sstream>
#include <string.h>
#include <math.h>
#include <time.h>
#include <stdlib.h>

#include "musique.h"

using namespace std;


int main()
{
	srand(time(NULL)); // initialisation de rand

	musique essai(0,0,0,0,0,0,-1,-1,1);
	essai.creation_musique();

	return 0;
}
