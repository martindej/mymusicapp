#ifndef DEF_MUSIQUE 
#define DEF_MUSIQUE

#include <iostream>
#include <fstream>
#include "instrument.h"
#include "note.h"
#include "motif.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <vector>

class modif;

class musique
{
	public:

	musique(int tonalite, int rythme, int duree_musique,int maj_min, int nbre_motifs,int duree_accords,int nbre_sol,int nbre_basse,int nbre_bat);
	//~musique();	

	void creation_musique();
	void set_partition(int oct, int no,int duree_note, int num_instrument, int sil,int volume,int note_1,int note_2,int note_3);
	void set_a(int num_instrument);
	int get_a(int num_instrument);
	int get_duree_musique();
	int get_duree_accords();
	std::vector<float> get_note(int temps, int num_instrument);
	int get_duree_note(int temps, int num_instrument);
	void choix_tonalite();
	void choix_rythme();
	void choix_duree();
	void choix_maj_min();
	unsigned long convert_endian_32(unsigned long input);
	unsigned short convert_endian_16(unsigned short input);
	unsigned long convert_endian(unsigned long input);
	unsigned long convert_endian_24(unsigned long input);
	void WriteVarLen(register unsigned long value, FILE *file_midi);
	void choix_nbre_motifs();
	void choix_duree_accords();
	void copy_suite_accords(std::vector < std::vector < int > > & accords_motif, int nbre);
	void add_parti(std::vector < std::vector < float > > parti, int c, int voie);
	void choix_pourc_sil();
	void write_midi();
	void remplissage_det_inst();
	void creation_vect_voies();
	int valeur_vect_voies(int i,int j);
	void choix_nbre_bat();
	void write_file();
	void choix_nbre_basse();
	void choix_nbre_sol();

	private:

	int tonalite;
	int rythme;
	int duree_musique;
	int maj_min;
	int nbre_motifs;
	int duree_accords;
	int nbre_inst;
	std::vector< int > a;
	std::vector< int > equilibre;
	std::vector< int > choix_inst;
	std::vector< float > note;
	std::vector < std::vector < std::vector < float > > > partition;
	std::vector < std::vector < int > > det_inst;
	int nbre_accords;
	std::vector < std::vector < int > > voies;
	motif* moti;
	instrument *inst;
	int nbre_sol;
	int nbre_basse;
	int nbre_bat;
	std::vector < std::vector < int > > accords;
};

#endif
