#ifndef DEF_MOTIF	 
#define DEF_MOTIF

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <vector>
#include "note.h"
#include "musique.h"

class musique;

class motif
{
	public:

	motif(int nbre_accords,int nbre_motifs,musique *pt);
	//~motif();	

	void suite_accords();
	int verif_accords(int d, int n);
	void choix_accords_disp();
	std::vector<std::vector<int> > get_accords();
	int get_nbre_accords();

	private:

	int nbre_accords;
	int nbre_motifs;
	musique *pt;
	std::vector < std::vector < int > > accords;
	std::vector < std::vector < int > > accords_disp;
	std::vector < std::vector < int > > div;
};

#endif
